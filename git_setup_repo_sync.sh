#!/bin/bash

# Setup the original repo from which we are forking (sync)

SYNC_REMOTE="https://github.com/nigels-com/glew.git"

git remote add sync $SYNC_REMOTE
git remote -v
git fetch sync
git branch --track orig-master sync/master

